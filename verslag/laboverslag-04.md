## Laboverslag: Troubleshooting

- Naam cursist: Sébastien Pattyn
- Bitbucket repo: https://bitbucket.org/sebastienpattyn/enterprise-linux-labo

### Procedures

1. Hardware troubleshooting
	- Stroom is aangesloten
	- kabels aangesloten
	- ip a => enp0s3 is UP, lo is ook UP, enp0s8 is DOWN
		
2. Netwerklaag
	- enp0s8 gebruikt geen dhcp, heeft statisch IP: 192.168.56.42 (juiste range)
	 => sudo ip link set dev enp0s8 (nu is enp0s8 UP)
	 => aanpassen enp0s8 onboot=yes
	- enp0s3 gebruikt DHCP
	- vbox NAT ip = 10.0.2.15
	- $ ip r => default gateway 10.0.2.2
	- $ sudo cat /etc/resolv.conf => DNS= 10.0.2.3
	- pingen naar default gateway lukt
	- dig www.google.com @8.8.8.8 + short => connection timed out, no service could be reached => PROBLEEM
		* in named.conf => aanpassing 192.168.56.in 56.168.192 (!niet in file)
		* aanpassing in 2.0.192.in-addr.arpa => punten zetten
		* aanpassing in 192.168.56.in-addr.arpa => origin aanpassen naar 56.168.192 ipv 192.168.56
	- named service kunnen nu gestart worden
	- verdere aanpassingen in cynalco.com
	=> butterfree overal 12 en beedle overal 13 (in cynalco en 192.168.56 file)
	=> onder mankey toevoegen van: files			IN  CNAME  mankey
	=> onder golbat.cynalco.com :     IN  NS tamatama.cynalco.com
	- $sudo systemctl restart named.service


3. Transportlaag
	- firewall aanpassen
	- sudo firewall-cmd --zone=public --permanent add-service=dns
	- sudo firewall-cmd --zone=public --permanent add-interface=enp0s8
	- restart firewall service



### Testplan en -rapport

- golbat.bats geeft 8 testen, O failures,
- dig kan vanaop host naar VM:
 => dig @192.168.56.42 mankey@cynalco.com

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- firewall instellingen gingen het vlotst
- interface aanpassen
- vinden dat de fout in DNS lag

#### Wat ging niet goed?

- zones aanpassen en de juiste fouten vinden.
- vooral de aanpassingen in cynalco.com, duurde lang

#### Wat heb je geleerd?

- beter leren werken met dig, en nslookup


#### Waar heb je nog problemen mee?

eenmaal alles gevonden is, lukte het en ben ik met alles mee.

### Referenties

https://www.centos.org/docs/5/html/Deployment_Guide-en-US/s1-bind-zone.html
http://www.liquidweb.com/kb/how-to-stop-and-disable-firewalld-on-centos-7/
https://www.centos.org/forums/viewtopic.php?t=7842

