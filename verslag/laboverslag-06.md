## Laboverslag: DHCP

- Naam cursist: Sébastien Pattyn
- Bitbucket repo: https://bitbucket.org/sebastienpattyn/enterprise-linux-labo

### Procedures
1. Als eerste voegen we pr001 toe aan vargant hosts met ip adres: 172.16.0.2
2. Erna voegen we aan ansible roles de DHCP Role toe. Deze bestaat uit:

File: roles/dhcp/handlers/main.yml
---
```
- name: Restart DHCP
  service: name={{ item }} state=restarted
  with_items:
    - dhcpd
  tags: dhcp
```

File: roles/dhcp/tasks/main.yml
---
```
- name: Install dhcp packages
  yum: pkg={{ item }} state=installed
  with_items:
    - dhcp
  tags: dhcp

- name: dhcp configuration
  template:
    dest=/etc/dhcp/dhcpd.conf
    src=dhcpd.conf.j2
  notify: Restart DHCP
  tags: dhcp

- name: Start dhcp service
  service: name={{ item }} state=started enabled=yes
  with_items:
    - dhcpd
  tags: dhcp

- name: Firewall rules for dhcp file share
  firewalld: service={{ item[0] }} permanent={{ item[1] }} state=enabled
  with_nested:
    - [ dhcp ]
    - [ true, false ]
  tags: dhcp
```

Dhcp.conf.j2
```
# Samba configuration, managed by Ansible. Please don't edit manually
# {{ ansible_managed }}
#
# vim: ft=samba

# create new
# specify domain name
option domain-name "{{ global_domain_name }}";

# specify name server's hostname or IP address
option domain-name-servers {{ global_domain_name_servers }};

# default lease time
default-lease-time {{ default_lease_time }};

# max lease time
max-lease-time {{ max_lease_time }};

# this DHCP server to be declared valid
{{ reliablity|default('authorative') }};

{% for subnet in subnets %}

# specify network address and subnet mask
subnet {{ subnet.ip }} netmask {{ subnet.netmask }} {
    option domain-name {{ subnet.domain_name }};
    # specify the range of lease IP address
    range dynamic-bootp {{ subnet.range }};
    # specify broadcast address
    option broadcast-address {{ subnet.broadcast }};
    # specify default gateway
    option routers {{ subnet.default_gateway }};
    # specify dns server
    option domain-name-servers {{ subnet.dns_servers }};
}

{% endfor %}

{% for host in reserved_ips %}

host {{ host.hostname }} {
  hardware ethernet {{ host.mac }};
  fixed-address {{ host.ip }};
  option host-name "{{ host.hostname }}";
}

{% endfor %}
```

3. We voegen pr001 ook toe in site.yml en als roles dhcp en bertvv.el7
4. Nu passen we pr001.yml aan 

```
global_domain_name: "linuxlab.lan"
global_domain_name_servers: "192.0.2.10, 192.0.2.11"
default_lease_time: 14400
max_lease_time: 28800

subnets:
  - domain_name: INTERNNETWERK
    ip: 172.16.0.0
    default_lease_time: 43200
    max_lease_time: 57600
    netmask: 255.255.0.0
    range: "172.16.100.1 172.16.255.253"
    broadcast: 172.16.255.255
    default_gateway: 172.16.255.254
    dns_servers: "192.0.2.10, 192.0.2.11"

reserved_ips:
  - hostname: testLinux
    mac: 08:00:27:61:a2:6c
    ip: 172.16.100.139
  - hostname: pr001
    mac: 08:00:27:60:f7:6c
    ip: 172.16.0.2
  - hostname: pr011
    mac: 08:00:27:a6:7a:28
    ip: 172.16.0.11
```
 
5. Als we vagrant provision runnen, worden deze toepassingen toegepast.


### Testplan en -rapport

1. Er is geen Test voor DHCP configuratie maar we hebben als reserved IP een VM testLinux die we gaan gebruiken om DHCP te testen
2. Deze virtuele machine gebruikt als host-Only adapter, dezelfde host-only als pr001
3. We kiezen dat de IP adressen automatisch moeten gegenereerd worden door DHCP
4. Wanneer We Ipconfig uitvoeren zien we dat onze Host-Only adapter het juiste IP krijgt dat in de pr001.yml staat.
```PowerShell
PS C:\Users\Administrator> ipconfig

Windows IP Configuration

Ethernet adapter Ethernet 2:

   Connection-specific DNS Suffix  . : INTERNNETWERK
   Link-local IPv6 Address . . . . . : fe80::5121:2142:9203:6107%13
   IPv4 Address. . . . . . . . . . . : 172.16.222.41
   Subnet Mask . . . . . . . . . . . : 255.255.0.0
   Default Gateway . . . . . . . . . : 172.16.255.254

Ethernet adapter Internet:

   Connection-specific DNS Suffix  . : hogent.be
   Link-local IPv6 Address . . . . . : fe80::114:10c4:162c:20ae%12
   IPv4 Address. . . . . . . . . . . : 10.0.2.15
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . : 10.0.2.2

```
5. We zien ook dat hij in INTERNETNETWERK zit dus dat is ideaal.

### Retrospectieve


#### Wat ging goed?
- Via de Role informatie, kijken hoe de pr001.yml moet aangepast worden. Ook de site.yml en vagrant_hosts aanpassen ging heel vlot.

#### Wat ging niet goed?
- Bij de role zat ik wat vast met de range van welke een vast IP krijgen en welke automatisch gegenereerd worden.

#### Wat heb je geleerd?
- Ik heb beter leren werken met DHCP server in te stellen op CentOs en hoe ik moet werken met Range en vaste IP's toekennen.

#### Waar heb je nog problemen mee?
- Momenteel heb ik nergens nog problemen mee.

### Referenties


