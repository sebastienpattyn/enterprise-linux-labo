## Laboverslag: 

- Naam cursist: Sébastien Pattyn
- Bitbucket repo: https://bitbucket.org/sebastienpattyn/enterprise-linux-labo

### Procedures

1. Stel de drie nieuwe vagrant-hosts op: pu001, pu002 en pu003. Open vagrant_hosts.yml en geef hier de hostnames en hun corrensponderend ip-adres.
2. Maak nu in de host_vars van ansible twee nieuwe yml-files met de naam van de nieuwe hosts (pu001.yml, pu002.yml en pu003.yml).
3. Installeer de role bind van https://github.com/bertvv/ansible-role-bind en zet deze in het mapje van roles.
Kopieer vervolgens het mapje 'filter_plugins' vanuit de role, naar het 'ansible'-mapje in ons geval. (dit is de home-folder van de ansible-directory)
4. pu001 functioneert als MasterDns.
In pu001.yml passen we de role van bind toe en komt het volgende te staan:
el7_firewall_allow_services:
  -  http
  -  https
  -  ssh
  -  dns

bind_allow_query:
  - 'any'

bind_listen_ipv4:
  - 'any'

bind_listen_ipv6:
  - 'any'

bind_zone_hosts:
#Master DNS
  - name: pu001
    ip: 192.0.2.10
    aliases:
      - ns1

#Slave DNS
  - name: pu002
    ip: 192.0.2.11
    aliases:
      - ns2

#Mail Server
  - name: pu003
    ip: 192.0.2.20
    aliases:
      - mail

#webserver LAMP
  - name: pu004
    ip: 192.0.2.50
    aliases:
      - www

#DHCP server
  - name: pr001
    ip: 172.16.0.1
    aliases:
      - dhcp

#LDAP-server
  - name: pr002
    ip: 172.16.0.2
    aliases:
      - directory

#Intranet (LAMP)
  - name: pr010
    ip: 172.16.0.10
    aliases:
      - inside

#Fileserver (Samba, FTP)
  - name: pr011
    ip: 172.16.0.11
    aliases:
      - files

bind_zone_networks:
  - "192.0.2"
  - "172.16"

bind_zone_name_servers:
  - 'pu001'
  - 'pu002'

bind_zone_master_server_ip: 192.0.2.10

bind_zone_name: 'linuxlab.lan'

bind_zone_mail_servers:
  - name: 'pu003'
    preference: '10'

5. pu002 functioneert als SlaveDns. Hierin komt het netwerkdomein waar uw dns zal runnen. In ons geval linuxlan.lab. Hier kennen we het ip-adres toe van de masterdns. Hier komen ook de namen van de twee dns-servers in het domein. Plus het netwerk dat deel uitmaakt van het domein. 
In pu002.yml komt het volgende te staan:
bind_allow_query:
  - 'any'

bind_listen_ipv4:
  - 'any'

bind_listen_ipv6:
  - 'any'

bind_allow_transfer:
  - '192.0.2.10'
  - '192.0.2.11'


bind_zone_name: 'linuxlab.lan'

bind_zone_master_server_ip: '192.0.2.10'

bind_zone_name_servers:
  - 'pu001'
  - 'pu002'

bind_zone_networks:
  - '192.0.2'
  - '172.16'

6. In de all.yml van de group_vars zetten we de firewallinstellingen, die http,https,ssh en dns toelaten.
el7_firewall_allow_services:
  -  http
  -  https
  -  ssh
  -  dns
7. in de site.yml, noteren we welke roles bij welke hosts moeten geinstalleerd zijn. Aangezien pu001 en pu002 hosts zijn die functioneren als dns-server, dan moeten we de bind-roles hieraan toevoegen samen met el7. pu003 functioneert als mailserver, om verdere problemen te voorkomen, gaan we deze nu al aanmaken.
Dus in de site.yml-file voegen we het volgende eraan toe:
- hosts: pu001
  sudo: true
  roles:
    - bertvv.el7
    - bertvv.bind

- hosts: pu002
  sudo: true
  roles:
    - bertvv.el7
    - bertvv.bind

- hosts: pu003
  sudo: true
  roles:
    - bertvv.el7

### Testplan en -rapport
- Vagrant up pu001, vagrant up pu002, vagrant up pu003, vagrant up pu004 moeten correct kunnen gerunt worden. zonder fouten.
=> alle 4 de hosts zijn correct ge-upt.
- Voor pu001,pu002 en pu004 host moeten de test-files correct overlopen worden, zonder failures.
=> pu001 test-file.
![screenschot pu001] (https://bytebucket.org/JensDeVreese93/enterprise-linux-labo/raw/6d1c4ff1e44b1439cad5c67a417132e7e0d5f5b1/Afbeeldingen/TestMasterDNS.PNG?token=d4a5ff33d70b335c3c211203e58db90a4f1c5279)
=> pu002 test-file:
![screenschot pu002] (https://bytebucket.org/JensDeVreese93/enterprise-linux-labo/raw/6d1c4ff1e44b1439cad5c67a417132e7e0d5f5b1/Afbeeldingen/TestSlaveDNS.PNG?token=b1922cdb022efc325bde8f99d3d8bc35be745ad1)
=> pu004 common test-file
![screenschot pu004common] (https://bytebucket.org/JensDeVreese93/enterprise-linux-labo/raw/6d1c4ff1e44b1439cad5c67a417132e7e0d5f5b1/Afbeeldingen/TestCommon.PNG?token=a689f9c6567bdbd2c623faa8ad27972b69f0c6e0)
=> pu004 lamp test-file
![screenschot pu004lamp] (https://bytebucket.org/JensDeVreese93/enterprise-linux-labo/raw/6d1c4ff1e44b1439cad5c67a417132e7e0d5f5b1/Afbeeldingen/TestLamp.PNG?token=82a890a24d725f5eef9d7830943de24b95f41065)

- Vanaf de host machine (in mijn geval windows 10) moet nslookup correct verlopen naar zowel de masterdns als naar de slavedns
vanaf host naar pu001 (masterdns) en vanaf host naar pu002 (slavedns)
![screenschot nslookup] (https://bytebucket.org/JensDeVreese93/enterprise-linux-labo/raw/6d1c4ff1e44b1439cad5c67a417132e7e0d5f5b1/Afbeeldingen/nslookupHost.PNG?token=78fab5fbd374c52053c93b88360cf247a580a69c)

- vanaf pu004 moet het mogelijk zijn om te kunnen diggen naar zowel de masterdns als de slavedns.
vanaf pu004 naar pu001 (masterdns)
![screenschot masterdns] (https://bytebucket.org/JensDeVreese93/enterprise-linux-labo/raw/6d1c4ff1e44b1439cad5c67a417132e7e0d5f5b1/Afbeeldingen/digPu001.PNG?token=2e25c92f96c633575cf55d7b357644f524578727)
vanaf pu004 naar pu002 (slavedns)
![screenschot slavedns] (https://bytebucket.org/JensDeVreese93/enterprise-linux-labo/raw/6d1c4ff1e44b1439cad5c67a417132e7e0d5f5b1/Afbeeldingen/digPu002.PNG?token=32e8d2f5e86a184f9369aae690a6afe8b5b8c606)

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- Eenmaal dat de BIND role geïnstalleerd was, was het via de readme en test.yml heel gemakkelijk om zelf het ansible playbook van de master op te zetten.

#### Wat ging niet goed?

- zoeken hoe we het best de slavedns moesten configureren. We zochten nog welke elementen er in de playbook moesten en welke niet.

#### Wat heb je geleerd?

- DNS opzetten en vooral door de troubleshoot van vorige week ging het heel vlot om alles te vinden van configuratiebestanden te vinden en dan aan te passen.

#### Waar heb je nog problemen mee?

- momenteel zijn er niet echt ergens problemen mee.

### Referenties

https://github.com/resmo/ansible-role-bind
http://renemoser.net/blog/2014/04/28/manage-bind-and-zones-files-using-ansible/