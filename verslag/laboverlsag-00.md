## Laboverslag: Opzetten Werkomgeving

- Naam cursist: Sébastien Pattyn
- Bitbucket repo: https://bitbucket.org/sebastienpattyn/enterprise-linux-labo

### Procedures

1. Installatie software

2. Aanmaken Bitbucket account
	$ git config --global user.name "sebastienpattyn"
	$ git config --global user.email ”sebastien.pattyn.s6523@student.hogent.be”
	$ git config --global push.default simple

3. ssh-sleutelpaar instellen
	$ ssh-keygen -t rsa
	Generating public/private rsa key pair.
	Enter file in which to save the key (/c/Users/sebas/.ssh/id_rsa):
	Enter passphrase (empty for no passphrase):
	Enter same passphrase again:
	Your identification has been saved in /c/Users/sebas/.ssh/id_rsa.
	Your public key has been saved in /c/Users/sebas/.ssh/id_rsa.pub.
	The key fingerprint is:
	SHA256:TeGNoW4sE7gWpAhty7rcTOaQn9ToN2KQzd1ZFCvgr+4 sebastien@DESKTOP-BGT0PSQ
	The key's randomart image is:
	+---[RSA 2048]----+
	|..  ..   .+      |
	|..oo...  +.=     |
	|.o..o...o.+ .    |
	|  o  o.+.+       |
	| .= * +.S .      |
	|.= O o.*         |
	|..% ..           |
	|.. X.o           |
	|  . +E.          |
	+----[SHA256]-----+

	Op bitbucket, sshkey invoeren

	$ ssh -T hg@bitbucket
	The authenticity of host 'bitbucket.org (131.103.20.167)' can't be established.
	RSA key fingerprint is SHA256:zzXQOXSRBEiUtuE8AikJYKwbHaxvSc0ojez9YXaGp1A.
	Are you sure you want to continue connecting (yes/no)? yes
	Warning: Permanently added 'bitbucket.org,131.103.20.167' (RSA) to the list of known hosts.
	logged in as sebastienpattyn.

	You can use git or hg to connect to Bitbucket. Shell access is disabled.


4. Creeër fork van repository, toevoegen repo aan spreadsheet, Permissies geven aan bertvanvreckem

5. Lokale kopie van repositoy
	$ git clone --config core.autocrlf=input git@bitbucket.org:sebastienpattyn/enterprise-linux-labo.git


6. aanpassen documenten in notepad
	$ cd enterprise-linux-labo
	$ git commit -am 'eerste commit'
	$ git push

7. Rollen installeren
	=> downloaden van role, uitpakken in juiste map, naam aanpassen naar bervv.el7, aanpassing in site.yml
		$ vi site.yml  => role toevoegen
		$ vagrant provision (zorg dat vagrant up is en in juiste directory zit)

8. Hosts configureren
	op host all.yml aanpassen
	common.bats => eigen naam gebruiken ipv bert en verwijderen van ^ voor ^epel

9. Testen
 inloggen op VM
 	$ sudo vagrant/test/runbats.sh



### Testplan en -rapport
0. Ga op het hostsysteem naar de directory met de lokale kopie van de repository.
1. Voer vagrant status uit
• je zou één VM moeten zien met naam pu004 en status not created. Als deze toch bestaat, doe dan eerst
vagrant destroy -f pu004.
2. Voer vagrant up pu004 uit.
• Het commando moet slagen zonder fouten (exitstatus 0)
3. Log in op de server met vagrant ssh srv010 en voer de acceptatietests uit:
[vagrant@pu004 test]$ sudo /vagrant/test/runbats.sh
Running test /vagrant/test/common.bats
✓ EPEL repository should be available
✓ Bash-completion should have been installed
✓ bind-utils should have been installed
✓ Git should have been installed
✓ Nano should have been installed
✓ Tree should have been installed
✓ Vim-enhanced should have been installed
✓ Wget should have been installed
✓ Admin user bert should exist
✓ Custom /etc/motd should be installed
10 tests, 0 failures
4. Log uit en log vanop het hostsysteem opnieuw in, maar nu met ssh. Er mag geen wachtwoord gevraagd worden.
$ ssh sebastien@192.0.2.50
Last login: Fri Oct  2 10:03:47 2015 from 192.0.2.1
Welcome to pu004.localdomain.
enp0s3     : 10.0.2.15         fe80::a00:27ff:fe5c:6428/64
enp0s8     : 192.0.2.50        fe80::a00:27ff:fe6b:501f/64
[vagrant@pu004 ~]$

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:


#### Wat ging goed?

De link met bitbucket verliep vlot in het begin, ook de roles installeren lukte goed, vagrant box aanmaken lukte ook goed.

#### Wat ging niet goed?

Heb enorm veel prbolemen gehad met mijn SSH-sleutelpaar, eerst een wachtwoord opgegeven en dan was het niet meer goed om zonder passwoord te connecteren me ssh. Na eens volledig opnieuw te beginnen, werkte alles goed.

#### Wat heb je geleerd?

Wanneer het niet lukt, gewoon blijven zoeken op internet en blijven proberen.

#### Waar heb je nog problemen mee?

Momenteel lukt alles, dus er zijn geen problemen.

### Referenties

https://github.com/bertvv/ansible-role-el7
http://docs.ansible.com/ansible/playbooks.html
