## Laboverslag: LAMP stack met Vagrant en Ansible

- Naam cursist: Sébastien Pattyn
- Bitbucket repo: https://bitbucket.org/sebastienpattyn/enterprise-linux-labo

### Procedures
1. Benodigde roles downloaden van github en uitpakken in ansible/roles
 => httpd, apache, wordpress, mariadb

2. aanpassen van site.yml
=> roles toevoegen en zorgen dat de naam juist is.

3. aanmaken van bestand pu004.yml in de host Vars.

httpd_scripting: 'php'
  
mariadb_databases:
  - wordpress

mariadb_users:
  - name: wordpress_usr
    password: SILvzu43
    priv: 'wordpress.*:ALL'

el7_firewall_allow_services:
  - http
  - https
  - ssh

el7_install_packages:
  - mod_ssl
  
mariadb_root_password: SILvzu43
wordpress_database: wordpress
wordpress_user: wordpress_usr
wordpress_password: SILvzu43

=> na deze instellingen zou normaal pu004 aangemaakt worden met alle benodigde roles en functies

4. Standaardcertificaat wijzigen.
 => op pu004:
 		$ yum install openssl
 		$ openssl genrsa -out ca.key 2048 
 		$ openssl req -new -key ca.key -out ca.csr
 		$ openssl x509 -req -days 365 -in ca.csr -signkey ca.key -out ca.crt
 		$ cp ca.crt /etc/pki/tls/certs
		$ cp ca.key /etc/pki/tls/private/ca.key
		$ cp ca.csr /etc/pki/tls/private/ca.csr

		=> vervolgens deze ook naar map op host kopieren.

		maak een nieuwe map aan in ansible role bertvv.httpd/files
		kopieer ca.crt, ca.key en ca.csr naar daar.
		aanpassing in bertvv.httpd/default/main.yml:

		httpd_SSLCertificateFile:  /etc/pki/tls/certs/ca.crt
		httpd_SSLCertificateKeyFile: /etc/pki/tls/private/ca.key

		aanpassing in bertvv.httpd/tasks/main.yml
		dit onderaan toegevoegd:

		- name: copy private key
		copy:
			src: ca.crt
			dest: /etc/pki/tls/certs
  		tags: private key
  
		- name: copy CSR
  		copy:
    		src: ca.csr
    		dest: /etc/pki/tls/private/ca.csr
  		tags: CSR

		- name: copy self signed key
  		copy:
    		src: ca.key
    		dest: /etc/pki/tls/private/ca.key
  		tags: CSR




### Testplan en -rapport

1. aanpassen van Lamp.bats
 => wijzigen van wordpress en mariadb gegevens.

 2. bij het runnen van lamp.bats krijgen we nog steeds foutmeldingen

 3. vagrant reload verhelpt een stuk van deze problemen.

 4. interface foutmelding niet opgelost gekregen
 => 
 Running test /vagrant/test/pu004/lamp.bats
 ✓ The necessary packages should be installed
 ✓ The Apache service should be running
 ✓ The Apache service should be started at boot
 ✓ The MariaDB service should be running
 ✓ The MariaDB service should be started at boot
 ✓ The SELinux status should be ‘enforcing’
 ✗ Firewall: interface enp0s8 should be added to the public zone
   (in test file /vagrant/test/pu004/lamp.bats, line 50)
     `firewall-cmd --list-all | grep 'interfaces.*enp0s8'' failed
 ✓ Web traffic should pass through the firewall
 ✓ Mariadb should have a database for Wordpress
 ✓ The MariaDB user should have "write access" to the database
 ✓ The website should be accessible through HTTP
 ✓ The website should be accessible through HTTPS
 ✓ The certificate should not be the default one
 ✓ The Wordpress install page should be visible under http://192.0.2.50/wordpress/
 ✓ MariaDB should not have a test database
 ✓ MariaDB should not have anonymous users

16 tests, 1 failure



### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:


#### Wat ging goed?

eenmaal ik doorhad dat ik alle roles op github vond, verliep het vlot, kijken in de readme hoe ik het playbook moest aanpassen.

#### Wat ging niet goed?

grootste moeilijkheid was de niet-default certificate oplossen, met wat hulp van andere studenten hebben we dit dan samen wel gevonden.

#### Wat heb je geleerd?

Ik versta nu het hele principe achter een LAMP-stack veel beter dan in het begin, en hoe alles automatiseren via ansible een heleboel werk uitspaart.

#### Waar heb je nog problemen mee?

hebben geprobeerd om zelf een role te schrijven voor ansible maar dat lukte nog niet zo goed.

### Referenties

https://github.com/bertvv/
http://docs.ansible.com/ansible/playbooks.html
https://wiki.centos.org/HowTos/Https
http://www.azavea.com/blogs/labs/2014/10/creating-ansible-roles-from-scratch-part-1/
