## Laboverslag: taak 7 Troubleshooting

- Naam cursist: Sébastien Pattyn
- Bitbucket repo: https://bitbucket.org/sebastienpattyn/enterprise-linux-labo

### Procedures

1. Controleren Link Layer

- Adapter zit aangesloten
- We zien In Virtual Box dat de kabels ook aangesloten zijn
- Het Commando ip link geeft aan dat alle interfaces UP zijn
=> geen problemen op de Link Layer

2. Network layer

2.1 IP addresss
- de interface settings van enp0s3 en enp0s8 zijn in orde.
- Interface zitten in de juiste IP range
- geen 169.254.x.x ip dus DHCP is niet het probleem.

2.2 default gateway
- ip r geeft geen foutmelding

2.3 DNS Server
- /etc/resolv.conf bestand is in orde => geen probleem met DNS

3. Transport Layer

3.1 Service and port
- httpd service is niet geïnstalleerd
=> sudo yum install httpd
=> sudo systemctl start httpd.service
 
 3.2 Firewall setting
 - Samba-client is nog geen toegelaten service op firewall
 - sudo firewall-cmd --permanent --add-service=samba
 - sudo firewall-cmd --reload

 4. Application Layer
 - problemen met share permissies
 - Alice moet ook aan charlie groep toegevoegd worden.



### Testplan en -rapport

- Als we de test runnen krijgen we fouten!
 => sudo systemctl start nmb
 => sudo setsebool -P samba_export_all_rw 1 

sudo chmod 775 /srv/shares/alpha 
sudo chmod 775 /srv/shares/bravo

groups alice
Aanpassing alice:su do usermod -a -G charlie alice


aanpassing samba.conf:
 Samba configuration, managed by Ansible. Please don't edit manually
# Ansible managed: /home/bert/CfgMgmt/troubleshooting/samba/ansible/roles/samba/templates/smb.conf.j2 modified on 2014-11-30 23:45:38 by bert on jace.asgard.lan
#
# vim: ft=samba

[global]
 # Server information
 netbios name = brokensamba
 workgroup = WORKGROUP
 server string = Fileserver %m

 # Logging
 syslog only = yes
 syslog = 1

 # Authentication
 security = user
 passdb backend = tdbsam
 map to guest = bad user

 # Name resolution: make sure \\NETBIOS_NAME\ works
 wins support = yes
 local master = yes
 domain master = yes
 preferred master = yes

[alpha]
  comment = alpha
  path = /srv/shares/alpha
  public = no
  write list = @alpha
  read list = @alpha
  valid users = @alpha
  directory mode = 0770

[bravo]
  comment = bravo
  path = /srv/shares/bravo
  public = no
  write list = @bravo
  read list = @bravo, alice
  directory mode = 0770

[charlie]
  comment = charlie
  path = /srv/shares/charlie
  public = no
  read list = @charlie
  directory mode = 0770

[delta]
  comment = delta
  path = /srv/shares/delta
  public = no
  write list = alice
  read list = @delta
  directory mode = 0770

[echo]
  comment = echo
  path = /srv/shares/echo
  public = no
  valid users = @echo

[foxtrot]
  comment = foxtrot
  path = /srv/shares/foxtrot
  public = no
  valid users = @foxtrot
  write list = @foxtrot
  read list = @foxtrot
  directory mode = 0770

  Nu slagen alle testen:

  [vagrant@brokensamba ~]$ sudo ./runbats.sh 
Running test /home/vagrant/samba.bats
 ✓ NetBIOS name resolution should work
 ✓ Shares should exist
 ✓ Check permissions of share alpha
 ✓ Check permissions of share bravo
 ✓ Check permissions of share charlie
 ✓ Check permissions of share delta
 ✓ Check permissions of share echo
 ✓ Check permissions of share foxtrot

8 tests, 0 failures


### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

- De aanpassingen in samba.conf lukten redelijk vlot. Firewall service ging ook goed, aangezien ik daar al veel aanpassingen in gedaan heb op vorige troubleshoot.

#### Wat ging niet goed?

- de fout in de sebool vinden. 

#### Wat heb je geleerd?

- Het is veel makkelijker om te sshn naar de virtuele machine en dan vanuit daar te werken. Dan is het niet nodig om keyboard aanpassingen te doen en kan je ook makkelijker bestanden bekijken en bewerken. Nu wisten we dat de fouten zaten in da samba shares maar heb ondanks toch de troubleshoot vanaf het begin gedaan aangezien dit enorm helpt, dan zag ik voorbeeld al meteen dat samba service niet was toegevoegd aan firewall.  

#### Waar heb je nog problemen mee?

Voor dit labo was ik nog niet zo goed op de hoogte van samba file server shares en aanpassingen van permissies, maar dit is nu volledig opgelost. Heb geen problemen meer op dat gebied.

### Referenties

- http://ss64.com/bash/chmod.html
- http://www.cyberciti.biz/faq/howto-linux-add-user-to-group/
