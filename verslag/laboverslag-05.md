## Laboverslag: 

- Naam cursist: Sébastien Pattyn
- Bitbucket repo: https://bitbucket.org/sebastienpattyn/enterprise-linux-labo

### Procedures

1. Bij vagrant_hosts pr011 toevoegen met het bijhorende ip adres : 172.16.0.11
2. Bij site.yml pr011 toevoegen met de roles voor samba, vsftpd en el7(voor de algemene instellingen).
3. De samba role was reeds beschikbaar dus die kon met gemak toegevoegd worden.
De role van vsftpd ziet er als volgt uit:
3 mappen:
- handlers
``` main.yml```
```
# roles/ftp/handlers/main.yml
---
- name: Restart FTP
  service: name=vsftpd state=restarted

- name: reload firewall
  service: name=firewalld state=reloaded
```
- tasks
```main.yml```
```
# roles/ftp/tasks/main.yml
---

- name: Install packages for FTP
  yum: name={{ item }} state=installed
  with_items:
    - vsftpd
  tags: ftp

- name: Set firewall rules
  firewalld:
    service=ftp
    state=enabled
    permanent={{ item }}
  with_items:
    - true
    - false
  tags: ftp

- name: Enable connection tracking
  lineinfile:
    dest=/etc/sysconfig/iptables-config
    regexp='^IPTABLES_MODULES='
    line='IPTABLES_MODULES="ip_conntrack_ftp"'
    state=present
  tags: ftp
  notify: reload firewall

#for local users, so that they have acces to their home dir
- name: Set selinux boolean ftp_home_dir
  seboolean: name=ftp_home_dir state=yes persistent=yes

- name: Set selinux boolean allow_ftpd_full_access
  seboolean: name=ftpd_full_access state=yes persistent=yes

- name: Configuration
  template:
    src=vsftpd.conf.j2
    dest=/etc/vsftpd/vsftpd.conf
    mode=0600
    owner=root
    group=root
  notify: Restart FTP
  tags: ftp

- name: Start FTP service
  service: name=vsftpd state=started enabled=true
  tags: ftp
```
- templates
```vsftpd.conf.j2```
```
# Vsftpd configuration
# {{ ansible_managed }}

# Anonymous login
anonymous_enable={{ ftp_anonymous_enable|default('YES') }}
{% if ftp_anon_root is defined %}
anon_root={{ ftp_anon_root }}
{% endif %}

# Registered user access
local_enable={{ ftp_local_enable|default('YES') }}
{% if ftp_local_root is defined %}
local_root={{ ftp_local_root }}
{% endif %}
local_umask={{ ftp_local_umask|default('022') }}
userlist_deny=YES

write_enable={{ ftp_write_enable|default('YES') }}

# Server port settings
connect_from_port_20=YES
listen={{ ftp_listen|default('YES') }}
listen_ipv6={{ ftp_listen_ipv6|default('NO') }}

pam_service_name=vsftpd

{% if ftp_config is defined %}
# Other settings
{% for key, value in ftp_config.iteritems() %}
{{ key }}={{ value }}
{% endfor %}
{% endif %}
```

4. In de host vars map een nieuwe yml file aanmaken voor pr011 en de instellingen voor de samba role aanvullen met alle shares en de samba users.
5. Ook een eigen user account aanmaken en de bijhorende read en write access toevoegen bij elke share en aan elke gebruiker toewijzen wat hij mag doen.
samba_shares:
  - name: directie
    mode: 775
    valid_users: '@directie, femkevdv'
    write_list: '@directie, @staf, @beheer'
    force_group: directie
    force_directory_mode: 775
    force_create_mode: 775
  - name: financieringen
    read only: no
    mode: 775
    valid_users: '@financieringen, @publiek'
    write_list: '@financieringen'
    force_group: financieringen
    force_directory_mode: 775
    force_create_mode: 775
  - name: verzekeringen
    mode: 775
    valid_users: '@verzekeringen, @publiek'
    write_list: '@verzekeringen'
    force_group: verzekeringen
    force_directory_mode: 775
    force_create_mode: 775
  - name: publiek
    mode: 775
    public: yes
    valid_users: '@publiek'
    write_list: '@publiek'
    force_group: publiek
    force_directory_mode: 775
    force_create_mode: 775
  - name: staf
    mode: 775
    valid_users: '@publiek'
    write_list: '@staf'
    force_group: staf
    force_directory_mode: 775
    force_create_mode: 775
  - name: beheer
    mode: 770
    valid_users: '@beheer'
    write_list: '@beheer'
    force_group: beheer
    force_directory_mode: 770
    force_create_mode: 770

samba_users:
  - name: franka
    given_name: Frank
    surname: Assengraaf
    groups:
      - directie
      - staf
      - financieringen
      - verzekeringen
      - publiek
    password: franka

  - name: femkevdv
    given_name: Femke
    surname: Van De Vorst
    groups:
      - staf
      - publiek
      - directie
    password: femkevdv
  - name: hansb
    given_name: Hans
    surname: Berghuis
    groups:
      - verzekeringen
      - publiek
    password: hansb

  - name: kimberlyvh
    given_name: Kimberly
    surname: Van Herk
    groups:
      - verzekeringen
      - publiek
    password: kimberlyvh

  - name: taniav
    given_name: Tania
    surname: Versantvoort
    groups:
      - verzekeringen
      - publiek
    password: taniav

  - name: peterj
    given_name: Peter
    surname: Jongsma
    groups:
      - financieringen
      - publiek
    password: peterj

  - name: maaiked
    given_name: Maaike
    surname: Doorn
    groups:
      - directie
      - financieringen
      - staf
      - verzekeringen
      - publiek
      - beheer
    shell: /bin/bash
    password: maaiked

  - name: sebastienp
    given_name: sebastien
    surname: Pattyn
    groups:
      - directie
      - financieringen
      - staf
      - verzekeringen
      - publiek
      - beheer
    shell: /bin/bash
    password: sebastien

samba_share_root: /srv/shares/
samba_enable_homes: 'yes'

samba_netbios_name: FILES
samba_workgroup: LINUXLAB

6. De ftp role was iets moeilijker maar deze heb ik kunnen gebruiken van een student van vorig jaar.
7. Weer in de pr011.yml file de bijhorende instellingen voor vsftpd aanvullen.

ftp_anonymous_enable: NO
ftp_local_root: /srv/shares/

8. controleren of de tests slagen en of de \\FILES beschikbaar was en of we naar ftp://172.16.0.11 konden surfen.


### Testplan en -rapport

1. Roles toevoegen en kijken of ze geinstalleerd worden.
Dit is gelukt aangezien er geen foutboutschappen waren voor de bind role.

2. samba.bats en vsftpd.bats runnen via runbats.sh.
Dit is gelukt aangezien er geen fouten meer waren.

3. Op de host machine naar \\FILES gaan en kijken of de shares te zien zijn en toegankelijk zijn.
Dit is gelukt en was toegankelijk door inloggen van een bepaalde gebruiker die enkel ingesteld was voor een bepaalde share.

4. Op de host machine naar ftp://172.16.0.11 gaan en eveneens kijken of de shares te zien zijn en of ze toegankelijk zijn.
Dit is gelukt aangezien de shares te zien waren en er ook moest ingelogt worden om toegang te krijgen.

5. Bestandspermissies controleren.
Een bestand is aangemaakt in een share die toegankelijk was voor de betreffende gebruiker. Daarna met een andere gebruiken die in dezelfde groep zit en toegang tot dezelfde share heeft dit bestand veranderen. Dit was ook gelukt.

6. Vagrant destroy en vagrant up doen van pr011 de testen laten runnen
pr011  staat op zonder errors en de testfile geeft geen erros meer aan.


### Retrospectieve

#### Wat ging goed?

-aanpassing YML files en hosts toevoegen


#### Wat ging niet goed?

- Vooral problemen gehad met femke. Het toekennen van permissies was lang geleden maar is ondertussen ook al beter. Uitloggen van de fileserver op Windows Is niet gemakkelijk en lukt nog altijd niet dus kon ik nog niet alles demonstreren.

#### Wat heb je geleerd?

Permissies op shares zitten er nu zeker goed in. eenmaal je weet hoe het in elkaar zit kan je het gemakkelijk zelf verder uitbreiden.

#### Waar heb je nog problemen mee?

Nog steeds niet gevonden hoe er moet uitgelogd worden op Windows.

### Referenties

- http://ubuntuforums.org/showthread.php?t=518293
- https://www.samba.org/samba/docs/man/Samba-HOWTO-Collection/AccessControls.html
- https://www.samba.org/samba/docs/man/manpages-3/smb.conf.5.html#CREATEMASK
- http://vsftpd.beasts.org/vsftpd_conf.html

